"""
Podane są 2 modele:

City: przechowujący rekordy miast w bazie danych
Flat: przechowujący rekordy mieszkań

Model City posiada 2 istotne pola, geo_area przechowujący obszar danego miasta w punktach oraz center, który jest
punktem (lat,lng) oznaczającym centrum miasta (niekoniecznie środkiem City.geo_area)

Model Flat posiada pole geo_point oznaczający koordynaty geograficzne (lat,lng) mieszkania

Zadaniem kandydata jest napisanie metody Flat.distance_from_city, która oblicza odległość mieszkania do najbliższego miasta.
Jeżeli mieszkanie znajduje się w mieście (i.e. Flat.geo_point znajduje się w którymkolwiek obszarze City.geo_area) to liczymy
odległość od centrum miasta.

Wymagania:
- Metoda musi zostać napisana *jednym zapytaniem SQL*, generowanym przez Django ORM (przy wykorzystaniu GeoDjango)
- można korzystać z metod pomocniczych (np. GeoQueryset.distance()), annotacji oraz agregacji

"""

from django.contrib.gis.db.models import MultiPolygonField, PointField
from django.db import models


class City(models.Model):
    geo_area = MultiPolygonField(verbose_name='Obszar na mapie')
    center = PointField(verbose_name='Punkt oznaczający centrum miasta')


class Flat(models.Model):
    geo_point = PointField(verbose_name='Koordynaty mieszkania na mapie')

    def distance_from_city(self):
        """
        Metoda obliczająca odległość mieszkania do najbliższego miasta.
        W przypadku jeżeli koordynaty mieszkania znajdują się w środku miasta, to liczy odległość do centrum tego miasta (City.center),
        w innym wypadku liczy odległość do granicy miasta (City.geo_area) znajdującego się najbliżej koordynatów mieszkania.
        Odległość jest zwracana w kilometrach (np. 3.1 km)
        """
        pass