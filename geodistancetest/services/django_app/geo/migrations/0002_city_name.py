# Generated by Django 2.0.6 on 2018-06-14 15:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='name',
            field=models.CharField(default='aaa', max_length=100, verbose_name='Nazwa'),
            preserve_default=False,
        ),
    ]
