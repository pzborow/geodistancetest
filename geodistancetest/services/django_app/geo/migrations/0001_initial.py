# Generated by Django 2.0.6 on 2018-06-14 11:58

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('geo_area', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4326, verbose_name='Obszar na mapie')),
                ('center', django.contrib.gis.db.models.fields.PointField(srid=4326, verbose_name='Punkt oznaczający centrum miasta')),
            ],
        ),
        migrations.CreateModel(
            name='Flat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('geo_point', django.contrib.gis.db.models.fields.PointField(srid=4326, verbose_name='Koordynaty mieszkania na mapie')),
            ],
        ),
    ]
